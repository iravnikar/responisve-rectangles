**What it does**

Responsive resizing - makes the content in 2-4 rectangles look beautiful and
aligned in all devices, they are getting narrower untill the limit of 480 px
(mobile phones) when they allign one after another. In case of 4 rectangles
there is another breaking point at 800px, when they allign themselves two by two.
If needed, one rectangle can have have two thirds of space and one just one
third that is left.



---

## How to use it

It's simple.

1. Copy css file responsive-resizing.
2. Add html code for rectangles in **body**.
3. And style them as you please.

---
